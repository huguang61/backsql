#coding:UTF-8
"""
mysql备份小脚本
@author:amin kira
version:1.0
时间：2017-08-29
"""
# 10 10 * * * /home/linux/test.py
import os,time,datetime
import MySQLdb
import logging
import sys

#数据库ip或者域名
host="192.168.17.194"
#数据库用户名
username="hg"
#数据库密码
password="1234"
#数据库端口
port="3306"


#备份目录
dirPath="/home/python/huguang"
#数据库备份保留天数
maxBackupDays=30
#在备份后删除过期备份，False则在备份前删除过期备份
deleteAfterBackup=True


def calculate_time():
    now = time.mktime(datetime.datetime.now().timetuple())
    result = time.strftime('%Y-%m-%d', time.localtime(now))
    return result
def deleteDir(path):
    "递归删除文件和文件夹"
    if(os.path.exists(path)):
        if(os.path.isdir(path)):
            fps=os.listdir(path)
            for fp in fps:
                deleteDir(path+"/"+fp)
            os.rmdir(path)
        else:
            os.remove(path)

def get_con(host="192.168.17.194", logsdb="test", user="hg", password="1234", port=3306):
    con = MySQLdb.connect(host=host, user=user, passwd=password, db=logsdb, port=port, charset='utf8')    # python中直接和数据库相连的语法。
    return con                              # 返回的是这个连接的对象

def delet_local_database(dbname):
    mylab = get_con()     # 获取和备份数据库之间的连接
    my = mylab.cursor()
    sql = "drop database if exists "+dbname                    # 使用删除数据库的语句
    my.execute(sql)
    my.close()
    mylab.close()
    print "本地数据库%s已经删除"%dbname

def create_local_database(dbname):
    mylab = get_con()           # 获取到的是连接对象。
    my = mylab.cursor()                                           # 这个也是创建一个直接能够操作数据库的执行语句。
    sql = "create database if not exists "+dbname                 # 这个就是创建数据库的语句
    my.execute(sql)
    my.close()
    mylab.close()
    print "本地数据库%s已经创建"%dbname                              # 这个就是创建数据库完毕了
def insert_art_to_database(dbname):
    mylab = get_con()
    my = mylab.cursor()
    sql = "use %s"%(dbname)
    my.execute(sql)
    sql = "set sql_safe_updates = 0"
    my.execute(sql)
    sql ="insert into testart.ddns select * from device where art is not null order by art desc"
    my.execute(sql)
    my.close()
    mylab.commit()
    mylab.close()
    print "art已从%s移入"%dbname
def insert_local_sql(date,dbname):                                 # 这个是向这里面插入数据
    global dirPath
    path = "%s/%s/%s.sql"%(dirPath,date,dbname)
    command = "mysql -u hg -p1 %s < %s"%(dbname, path)            # 这个语句是插入文件
    os.system(command)


def deleteTooManyBackup():
    "删除过多的备份"
    global maxBackupDays, dirPath                 # 获取到最大的备份时间
    obj=datetime.datetime.now()-datetime.timedelta(days=maxBackupDays-1)    # 用现在的时间减去29天
    title=obj.strftime("%Y%m%d")
    old_dbname = str(title)+"_ddns_mac"         # 定义旧版本的时间名字
    delet_local_database(old_dbname)            # 删除旧版本
    fps=os.listdir(dirPath)                     # 获取到备份地址
    for temp in fps:                            # 获取到旧版本的
        if(int(temp)<int(title)):
            deleteDir(dirPath+"/"+temp)          # 递归删除文件

def index(db,d):
    "利用mysqldump备份数据库"
    global host, username, password, port, dirPath                # 这个代码就是默认的路径
    #mysqldump -h ip -P 端口 -u 用户名 -p密码 数据库名字 | gzip > 备份目录/当前日期/时间_数据库名字.sql.gz
    dbname = "%s_%s"%(d,db)                      #  将时间和名字联合在一起
    command="mysqldump -h %s -P %s -u %s -p%s %s > %s/%s/%s_%s.sql"%(host,port,username,password,db,dirPath,d,d,db)    # 执行这个命令
    os.system(command)                               # 用系统来执行这个命令
    print "备份文件生成完毕!"
    create_local_database(dbname)                    # 创建一个本地数据库
    print "数据库%s创建完毕"%(dbname)
    insert_local_sql(d,dbname)
    print "数据表加入成功"


def task():
    "入口函数"
    global deleteAfterBackup, dirPath                  # 是否是在备份前删除，以及备份的路径。
    dbList = ['test']
    sheet_time = datetime.datetime.now()
    d = sheet_time.strftime('%Y%m%d')
    print "备份时间为",d        # 答应备份时间
    path="%s/%s"%(dirPath, d)                # 获取备份路径和备份时间
    if(not os.path.exists(path)):            # 如果路径不存在
        os.makedirs(path)                    # 创建递归目录树

    if(not deleteAfterBackup):
        deleteTooManyBackup()                # 这个函数就是删除已有的备份
    for dbName in dbList:                    # 一个设列表，一个是时间
        index(dbName,d)                      # 这个就是创建已有函数目录
    if(deleteAfterBackup):
        deleteTooManyBackup()



if __name__ == "__main__":
    reload(sys)           # 相当于导入这个模块
    sys.setdefaultencoding('utf-8')    # 设定编码格式
    task()
